import java.util.Scanner;

public class PalindromeTest {

	private static Scanner input;

	public static void main(String[] args) 
	{
		input = new Scanner(System.in);
		
		Palindrome testword = new Palindrome("");
		
		String inputword;
		
		System.out.println("Hello person!\nPlease enter a potential palindrome");
		inputword = input.nextLine();
		
		testword.setWord(inputword);
		
		System.out.println(testword.toString());
	
	}

}
