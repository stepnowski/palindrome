
public class Palindrome 
{
  private String word;
  private int wordlength;
    
  //constructor takes initial word 
  public Palindrome(String userWord)
  {
	  word = userWord; 
  }
  
  //setter method to set word
  public void setWord(String newWord)
  {
	  word = newWord;
  }
  
  public String getWord()
  {
	  return word;
  }
  
  public String getReverse()
  {
	  String holder = "";
	  wordlength = word.length();
	  for(int i=1;i<=wordlength;i++)
	  {
		  holder = holder + word.substring(wordlength - i, wordlength - i + 1);
	  }
	  return holder;
  }
  
  public boolean isPalindrome()
  {
	  boolean result;
	  
	  if (word.equalsIgnoreCase(getReverse()))
		  result = true;
	  else
		  result = false;
	  
	  return result;
  }
  
  public String toString()
  {
	  String answer;
	  if (isPalindrome())
		  answer = String.format("%s is a palindrome", word);
	  else
		  answer = String.format("%s is not a palindrome", word);
  
      return answer;  
  }
}// end class Palindrome
